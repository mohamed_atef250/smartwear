package com.yumaas.smartwear;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import nl.joery.animatedbottombar.AnimatedBottomBar;

public class MainActivity extends AppCompatActivity {
    TextView title;
    AnimatedBottomBar smoothBottomBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        smoothBottomBar=findViewById(R.id.bottom_bar);
        title = findViewById(R.id.title);

        FragmentHelper.addFragment(MainActivity.this,new ChatUsersFragment(),"ChatUsersFragment");

        findViewById(R.id.delete).setOnClickListener(view -> logOut());
        findViewById(R.id.title_).setOnClickListener(view -> logOut());

                smoothBottomBar.setOnTabSelectListener(new AnimatedBottomBar.OnTabSelectListener() {
                    @Override
                    public void onTabSelected(int i, @Nullable AnimatedBottomBar.Tab tab, int i1, @NotNull AnimatedBottomBar.Tab tab1) {
                        FragmentHelper.popAllFragments(MainActivity.this);

                        if (i1 == 0) {
                            FragmentHelper.addFragment(MainActivity.this, new ChatUsersFragment(), "ChatUsersFragment");
                        } else if (i1 == 1) {
                            FragmentHelper.addFragment(MainActivity.this, new AddChildFragment(), "AddChildFragment");

                        } else if (i1 == 2) {
                            FragmentHelper.addFragment(MainActivity.this, new ProfileFragment(), "ProfileFragment");
                        }
                        title.setText(tab1.getTitle());
                    }

                    @Override
                    public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

                    }
                });
    }


    private void logOut(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("هل تريد تسجيل الخروج ؟");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "نعم",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "غلق",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}