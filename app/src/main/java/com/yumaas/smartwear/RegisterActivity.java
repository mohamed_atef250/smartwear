package com.yumaas.smartwear;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yumaas.smartwear.base.Validate;
import com.yumaas.smartwear.databinding.FragmentRegisterBinding;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    FragmentRegisterBinding fragmentRegisterBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentRegisterBinding = DataBindingUtil.setContentView(this, R.layout.fragment_register);

        FirebaseApp.initializeApp(this);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جار التحميل");


        fragmentRegisterBinding.register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(fragmentRegisterBinding.name.getText().toString(),
                        fragmentRegisterBinding.email.getText().toString(),
                        fragmentRegisterBinding.password.getText().toString(),
                        fragmentRegisterBinding.phone.getText().toString());
                user.type=2;
                progressDialog.show();
                writeNewUser(user);
                progressDialog.cancel();
                SweetDialogs.successMessage( RegisterActivity.this, "تم التسجيل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        Intent intent = new Intent( RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });






        mDatabase = FirebaseDatabase.getInstance().getReference();






    }

    public void writeNewUser(User user) {

        mDatabase.child("users").push().setValue(user);
    }



    private boolean validate(){

        if(Validate.isEmpty(fragmentRegisterBinding.name.getText().toString())){
            fragmentRegisterBinding.name.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.email.getText().toString())){
            fragmentRegisterBinding.email.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.phone.getText().toString())){
            fragmentRegisterBinding.phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.password.getText().toString())){
            fragmentRegisterBinding.password.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(fragmentRegisterBinding.email.getText().toString())){
            fragmentRegisterBinding.email.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }


}