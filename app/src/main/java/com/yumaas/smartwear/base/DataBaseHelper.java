package com.yumaas.smartwear.base;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import com.yumaas.smartwear.User;
import com.yumaas.smartwear.volleyutils.MyApplication;


import java.util.ArrayList;


public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance() {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }




    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static void saveStudent(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(student);
        prefsEditor.putString("savedUser", json);
        prefsEditor.apply();
    }









}
