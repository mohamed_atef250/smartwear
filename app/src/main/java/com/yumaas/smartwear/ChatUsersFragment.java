package com.yumaas.smartwear;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yumaas.smartwear.base.DataBaseHelper;

import java.util.ArrayList;


public class ChatUsersFragment extends Fragment {

    View rootView;
    private DatabaseReference mDatabase;
    ProgressDialog progressDialog;
    ValueEventListener valueEventListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_children, container, false);

        FirebaseApp.initializeApp(getActivity());
        mDatabase = FirebaseDatabase.getInstance().getReference();

        ArrayList<User> images = new ArrayList<>();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جار التحميل");
        User currentUser= DataBaseHelper.getSavedUser();
        DatabaseReference reference = mDatabase.child("childs").getRef();

        valueEventListener =   reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                progressDialog.cancel();
                progressDialog.dismiss();

                for (DataSnapshot ds: dataSnapshot.getChildren()) {

                    try {
                        User modelClass = (ds.getValue(User.class));
                        modelClass.key=ds.getKey();

                        if(modelClass.fatherKey!=null
                                &&currentUser.key.equals(modelClass.fatherKey))
                        images.add(modelClass);


                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }

                final ChatUsersAdapter chatUsersAdapter = new ChatUsersAdapter(new OnItemClickListener() {
                    @Override
                    public void onItemClickListener(int position) {

                    }
                }, images,reference);
                final RecyclerView programsList = rootView.findViewById(R.id.places_list);
                ViewOperations.setRVHVertical(getActivity(), programsList);
                programsList.setAdapter(chatUsersAdapter);

                reference.removeEventListener(valueEventListener);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });






        return rootView;
    }
}