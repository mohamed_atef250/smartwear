package com.yumaas.smartwear;


import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ChatUsersAdapter extends RecyclerView.Adapter<ChatUsersAdapter.ViewHolder> {
    DatabaseReference databaseReference;
    OnItemClickListener onItemClickListener;
    ArrayList<User> users;


    public ChatUsersAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users, DatabaseReference databaseReference) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;
        this.databaseReference=databaseReference;

    }


    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }


    @Override
    public ChatUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        ChatUsersAdapter.ViewHolder viewHolder = new ChatUsersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ChatUsersAdapter.ViewHolder holder, final int position) {
        holder.name.setText(users.get(position).username);
        holder.phone.setText(users.get(position).phone);
        try {
            Picasso.get().load(users.get(position).image).into(holder.imageView);
        }catch (Exception e){
            e.getStackTrace();
        }
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),MapActivity.class);
                intent.putExtra("lat",users.get(position).lat);
                intent.putExtra("lng",users.get(position).lng);
                intent.putExtra("id",position);
                view.getContext().startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(view -> {

            new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح هذا الطفل ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                databaseReference.child(users.get(position).key).removeValue();
                users.remove(position);
                notifyDataSetChanged();

                try {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                }catch (Exception e){
                    e.getStackTrace();
                }

            }).show();
        });


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,delete;
        Button btn;
        TextView name,phone;


        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            btn=view.findViewById(R.id.btn);
            name=view.findViewById(R.id.name);
            phone=view.findViewById(R.id.phone);
            delete=view.findViewById(R.id.delete);


        }
    }
}