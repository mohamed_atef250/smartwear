package com.yumaas.smartwear;

public class User {

    public String username;
    public String email,password,phone,image;
    public String key="",fatherKey="";
    public double lat=0.0,lng=0.0;
    public int type;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email,String password,String phone) {
        this.username = username;
        this.password=password;
        this.phone=phone;
        this.email = email;
    }


}
