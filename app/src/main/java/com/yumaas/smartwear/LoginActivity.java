package com.yumaas.smartwear;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yumaas.smartwear.base.DataBaseHelper;


public class LoginActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    EditText email, password;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},123);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جار التحميل");



        FirebaseApp.initializeApp(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                login();
            }
        });
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);

                startActivity(intent);
            }
        });


        findViewById(R.id.login_child).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                loginChild();
            }
        });


    }

    ValueEventListener valueEventListener = null;

    private void login() {

        DatabaseReference reference = mDatabase.child("users").getRef();

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isFound = false;
                progressDialog.cancel();
                progressDialog.dismiss();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    try {
                        User modelClass = (ds.getValue(User.class));

                        if (email.getText().toString().equals(modelClass.email)
                                && password.getText().toString().equals(modelClass.password)) {

                            modelClass.key = ds.getKey();
                            DataBaseHelper.saveStudent(modelClass);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                            startActivity(intent);
                            finish();


                            if (valueEventListener != null)
                                reference.removeEventListener(valueEventListener);

                            return;

                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    // fetch any key value pair
                }

                if (!isFound) {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        reference.addValueEventListener(valueEventListener);
        //reference.removeEventListener(valueEventListener);
    }

    boolean loginned = false;

    private void loginChild() {

        DatabaseReference reference = mDatabase.child("childs").getRef();

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isFound = false;
                if (!loginned) {

                    progressDialog.cancel();
                    progressDialog.dismiss();


                    Log.e("dasmota", "OIT");
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {


                        User modelClass = (ds.getValue(User.class));

                        Log.e("dasmota", "" + modelClass.email + "   " + modelClass.password);

                        if (email.getText().toString().equals(modelClass.email)
                                && password.getText().toString().equals(modelClass.password)) {

                            modelClass.key = ds.getKey();
                            DataBaseHelper.saveStudent(modelClass);

                            Log.e("dasmota", modelClass.toString());

                            Intent intent = new Intent(LoginActivity.this, ChildMapActivity.class);

                            startActivity(intent);
                            finish();


                            if (valueEventListener != null)
                                reference.removeEventListener(valueEventListener);
                            loginned = true;
                            return;

                        }


                        // fetch any key value pair
                    }
                }
                if (!isFound) {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        reference.addValueEventListener(valueEventListener);
    }


}