package com.yumaas.smartwear;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yumaas.smartwear.base.DataBaseHelper;
import com.yumaas.smartwear.base.Validate;
import com.yumaas.smartwear.databinding.FragmentProfileBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ProfileFragment extends Fragment {

    View rootView;
    private DatabaseReference mDatabase;
    FragmentProfileBinding fragmentProfileBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        FirebaseApp.initializeApp(getActivity());

        mDatabase = FirebaseDatabase.getInstance().getReference();

        User user2 = DataBaseHelper.getSavedUser();

        fragmentProfileBinding.name.setText(user2.username);
        fragmentProfileBinding.email.setText(user2.email);
        fragmentProfileBinding.phone.setText(user2.phone);
        fragmentProfileBinding.password.setText(user2.password);

        fragmentProfileBinding.register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(fragmentProfileBinding.name.getText().toString(),
                        fragmentProfileBinding.email.getText().toString(),
                        fragmentProfileBinding.password.getText().toString(),
                        fragmentProfileBinding.phone.getText().toString());
                user.type=2;
                user.key=user2.key;
                DataBaseHelper.saveStudent(user);

                mDatabase.child("users").child(user2.key).setValue(user);

                SweetDialogs.successMessage(getActivity(), "تم تعديل البيانات بنجاح", sweetAlertDialog -> {

                });
            }
        });

        rootView = fragmentProfileBinding.getRoot();
        return rootView;
    }


    private boolean validate(){

        if(Validate.isEmpty(fragmentProfileBinding.name.getText().toString())){
            fragmentProfileBinding.name.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(fragmentProfileBinding.email.getText().toString())){
            fragmentProfileBinding.email.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(fragmentProfileBinding.phone.getText().toString())){
            fragmentProfileBinding.phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(fragmentProfileBinding.password.getText().toString())){
            fragmentProfileBinding.password.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(fragmentProfileBinding.email.getText().toString())){
            fragmentProfileBinding.email.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }
}