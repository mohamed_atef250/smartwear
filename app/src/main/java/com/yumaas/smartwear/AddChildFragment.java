package com.yumaas.smartwear;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yumaas.smartwear.base.DataBaseHelper;
import com.yumaas.smartwear.base.Validate;
import com.yumaas.smartwear.base.filesutils.FileOperations;
import com.yumaas.smartwear.base.filesutils.VolleyFileObject;
import com.yumaas.smartwear.databinding.FragmentAddChildBinding;
import com.yumaas.smartwear.volleyutils.ConnectionHelper;
import com.yumaas.smartwear.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AddChildFragment extends Fragment {

    View rootView;
    FragmentAddChildBinding fragmentAddChildBinding;
    private DatabaseReference mDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAddChildBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_child, container, false);


        FirebaseApp.initializeApp(requireActivity());

        final ProgressDialog progressDialog = new ProgressDialog(requireActivity());
        progressDialog.setTitle("جار التحميل");

        fragmentAddChildBinding.image.setFocusable(false);
        fragmentAddChildBinding. image.setClickable(true);

        fragmentAddChildBinding. image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });


        fragmentAddChildBinding.register.setOnClickListener(view -> {
            if (validate()) {
                User user = new User(fragmentAddChildBinding.name.getText().toString(),
                        fragmentAddChildBinding.email.getText().toString(),
                        fragmentAddChildBinding.password.getText().toString(),
                        fragmentAddChildBinding.phone.getText().toString());

                user.image=selectedImage;
                user.type = 2;
                user.fatherKey = DataBaseHelper.getSavedUser().key;
                progressDialog.show();
                writeNewUser(user);
                progressDialog.cancel();
                SweetDialogs.successMessage(requireActivity(), "تم اضافه طفلك بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        FragmentHelper.popAllFragments(getActivity());
                        FragmentHelper.addFragment(getActivity(),new ChatUsersFragment(),"ChatUsersFragment");
                    }
                });
            }
        });


        mDatabase = FirebaseDatabase.getInstance().getReference();


        rootView = fragmentAddChildBinding.getRoot();

        return rootView;
    }

    public void writeNewUser(User user) {

        mDatabase.child("childs").push().setValue(user);
    }


    private boolean validate() {

        if (Validate.isEmpty(fragmentAddChildBinding.name.getText().toString())) {
            fragmentAddChildBinding.name.setError("ادخل الاسم");
            return false;
        } else if (Validate.isEmpty(fragmentAddChildBinding.email.getText().toString())) {
            fragmentAddChildBinding.email.setError("ادخل البريد الالكتروني");
            return false;
        } else if (Validate.isEmpty(fragmentAddChildBinding.phone.getText().toString())) {
            fragmentAddChildBinding.phone.setError("ادخل رقم الهاتف");
            return false;
        } else if (Validate.isEmpty(fragmentAddChildBinding.password.getText().toString())) {
            fragmentAddChildBinding.password.setError("ادخل كلمه المرور");
            return false;
        } else if (!Validate.isMail(fragmentAddChildBinding.email.getText().toString())) {
            fragmentAddChildBinding.email.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }



    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        fragmentAddChildBinding.image.setText("تم اضافه الصوره بنجاح");

        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }






}